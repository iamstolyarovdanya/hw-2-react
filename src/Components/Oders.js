import React, { useState } from 'react';
import { MdDeleteOutline } from "react-icons/md";
import PropTypes from 'prop-types';
import Modal from './Modal';
function Oders({item ,deleteOder , approveDeleteOders  , approveDeleteOdFunc }) {
const [deleteModal , setDeleteModal] = useState(false)
const isDeleteHand = ()=>{
    setDeleteModal(!deleteModal)
}
    return (
        <div className='Oders-wrapper'>
            {
                 item.length > 0 ?
                item.map((el)=>( 
                
                    <div className="buy-item" key={el.id}>
                        <img src={"images/" + el.img} alt="oders"/>
                    <h2 >{el.name}</h2>
                    <p>color: {el.color}</p>
                        <b>price : {el.price} $</b>
                        <MdDeleteOutline className='delete' onClick={()=> {
                            approveDeleteOdFunc(el)
                            isDeleteHand()
                        }}/>
                    </div> 
                )): <div className='Nothing'> Did you chose something? No.</div>
            }
              {deleteModal && <Modal Modal_text="Do you want delete this item from buy list?"
        titleName="Delete"
        isDeleteHand={isDeleteHand}
        deleteOder={deleteOder}
        approveDeleteOders={approveDeleteOders}
        isModalFor={"Delete"} 
         
           />}

        </div>
    );
}
Oders.propTypes = {
    item : PropTypes.array.isRequired,
  
    }

export {Oders};