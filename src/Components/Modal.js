import React from 'react';
import PropTypes from 'prop-types';
import Button from './Button';
function Modal({ Modal_text, titleName, modalToogle, chosee, mainOder,  changeStanBtn , changeStanBtnApp , isModalFor,isDeleteHand ,deleteOder  ,  approveDeleteOders}) {
    return (
        <div>
            <div id="blur" className="blur" onClick={(e) => (
                e.target.id === "blur" && (isModalFor === "Approve" ? modalToogle() : isDeleteHand() )
            )}>

                <div className="modal">
                    <h2 className="modal__title">{titleName}</h2>
                    <p>{Modal_text}</p>

                   { isModalFor === "Approve" && <div className="modal__btn-wrapper">
                        <Button 
                        chosee={chosee} mainOder={mainOder} modalToogle={modalToogle} isBtnFor="addToBuyList" 
                         changeStanBtnApp={changeStanBtnApp}  changeStanBtn={ changeStanBtn}/>
                       <Button chosee={chosee} mainOder={mainOder} modalToogle={modalToogle} isBtnFor="closeModal"/>
                    </div>
                     }
                        { isModalFor === "Delete" && <div className="modal__btn-wrapper">
                        <Button 
                        deleteOder={deleteOder} approveDeleteOders={approveDeleteOders} isDeleteHand={isDeleteHand} isBtnFor="Delete_Oder" 
                         changeStanBtnApp={changeStanBtnApp}/>
                       <Button chosee={chosee} mainOder={mainOder} isDeleteHand={isDeleteHand} isBtnFor="closeModalDelete"/>
                    </div>
                     }
                </div>
            </div>
        </div>
    );
}
Modal.propTypes = {
    Modal_text: PropTypes.string,
    titleName: PropTypes.string,
  
    modalToogle: PropTypes.func,
}
export default Modal;