import React, { useState, useEffect } from 'react';
import Modal from "./Modal";
import { FaRegStar } from "react-icons/fa";
import PropTypes from 'prop-types';
import Button from './Button';



function CardItem({ itemCard, Chose, clearMainChoose, mainChoose, mainoders, starListAdd, deleteStarList , oders , stanBtnApp, starList}) {
    const [addStar, setaddStar] = useState(false)
    const [stanBtn , setStanBtn] = useState(false)
    const [modal, setModal] = useState(false)


    const modalToogle = () => {
      setModal(!modal)
  }

    const changeStanBtn = ()=>{
     setStanBtn(true )

    }
    const change = () => {
        setaddStar(!addStar)

    }
    useEffect(() => {
        const starData = JSON.parse(localStorage.getItem('star'));
        if (starData) {
          starData.forEach((element) => {
            if (element.id === itemCard.id) {
              change();
            }
          });
        }
      }, []);
      useEffect(() => {
        const starData = JSON.parse(localStorage.getItem('oders'));
        if (starData) {
          starData.forEach((element) => {
            if (element.id === itemCard.id) {
              changeStanBtn()
            }
          });
        }
      }, []);
    return (
    <>
    <div className="card_item">

        <FaRegStar className={` ${(addStar) && `star-active`}`} onClick={() => {
           change();
           !addStar ?  starListAdd(itemCard) : deleteStarList(itemCard)
        }} />
        <h2 > {itemCard.name}</h2>
        <b>price : {itemCard.price} $</b>
        <img src={"images/" + itemCard.img} width="160" height="155" alt="photoww" />
        <p>color : {itemCard.color}</p>
       {stanBtn && <p id='already-In-Buy-list' >Вже у корзині </p>}
        <Button  isBtnFor="addToBuy" changeStanBtn={changeStanBtn} modalToogle={modalToogle} mainChoose={mainChoose} itemCard={itemCard} stanBtn={stanBtn}  stanBtnApp={stanBtnApp}/>
    </div >
 {modal && <Modal Modal_text="Do you want add this item in buy list?"
 titleName="Approve"
     modalToogle={modalToogle}
     chosee={Chose}
     mainOder={mainoders}
     clearMainOder={clearMainChoose}
   isModalFor={`Approve`}
   changeStanBtn={ changeStanBtn}
    />}
    </>
    );
}
CardItem.propTypes = {
    itemCard : PropTypes.object.isRequired,
    mainoders: PropTypes.array.isRequired,
    Chose: PropTypes.func.isRequired,
    clearMainChoose: PropTypes.func.isRequired,
    starListAdd: PropTypes.func.isRequired,
    deleteStarList: PropTypes.func.isRequired,
    mainChoose:PropTypes.func.isRequired,
    
    }
export default CardItem






