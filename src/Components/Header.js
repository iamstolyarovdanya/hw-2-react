import React, { useState } from 'react';
import { MdOutlineShoppingCart } from "react-icons/md";
import Oders from "./Oders";
import { FaRegStar } from "react-icons/fa";
import Star from "./Star"
import PropTypes from 'prop-types';
import { Link, Outlet  , } from 'react-router-dom';
function Header({ oders, starList, deleteOder, deleteStarList }) {
    const [cardOpen, setCardOpen] = useState(false)
    const [cardOpenStar, setCardOpenStar] = useState(false)
    
    return (
        <>
            <div className="header">
                <span className="logo"><Link  to="/">Jewelry Paradise</Link></span>
                <ul >
                    <p className='oder_counter'>{oders.length}</p>

                    <Link to="/Shop"> <MdOutlineShoppingCart className={` icon-buy `}/></Link>
                 
                    <p className='star_counter'> {starList.length}</p>
                    <Link to="/Star"> <FaRegStar  className={` icon-buy `}/></Link>
                  
                    <li>About as</li>
                    <li>Contacts</li>
                </ul>
               
            </div>
            <main>
                <Outlet />
            </main>
        </>
    );
}
Header.propTypes = {
    oders: PropTypes.array.isRequired,
    starList: PropTypes.array.isRequired,
    deleteOder: PropTypes.any.isRequired,
    deleteStarList: PropTypes.any.isRequired,
}
export { Header};