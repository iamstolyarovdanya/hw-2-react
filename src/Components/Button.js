import React, { useState } from 'react'

const Button = ({chosee, mainOder  , modalToogle , isBtnFor , mainChoose , itemCard , stanBtn ,  isDeleteHand , approveDeleteOders , deleteOder , changeStanBtn}) => {
 const [show , setShow] = useState(false)
  return (
    <>
    {
        isBtnFor === `addToBuy` &&  <button    disabled={stanBtn} className={` ${(stanBtn) && `btn-unactive`}`}
        onClick={() => {            
                modalToogle();
                mainChoose(itemCard)
               
       }}>Добавить в корзину</button>
    }
     {
        isBtnFor === `addToBuyList` &&   <button onClick={() => {
                           
            chosee(...mainOder)
            modalToogle();
            changeStanBtn()      
        }} >OK</button>
    }
     {
        isBtnFor === `closeModal` &&   <button onClick={() => {
            modalToogle();
           ;
           
        }}>Cansel</button>
    }
     {
        isBtnFor === `closeModalDelete` &&   <button onClick={() => {
          isDeleteHand();
           ;
           
        }}>Cansel</button>
    }
     {
        isBtnFor === `Delete_Oder` &&   <button onClick={() => {
                           
          deleteOder(approveDeleteOders)
          isDeleteHand();
    
        }} >OK</button>
    }
    </>
  )
}

export default Button