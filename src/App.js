import './App.css';
import React, { useEffect, useState } from "react";
import {Card} from "./Components/Card";
import {Header} from "./Components/Header";
import Modal from "./Components/Modal";
import {Oders} from './Components/Oders';
import {Star} from './Components/Star';
import { Route , Routes , Link } from 'react-router-dom';

function App() {
    const [mainoders, SetMainOders] = useState([])
    const [oders, setOders] = useState([])
    const [item, setItem] = useState()
    const [starList, setStarList] = useState([])
    const [approveDeleteOders  , setApproveDeleteOders] = useState()
  
  
    useEffect(() => {
        setOders(JSON.parse(localStorage.getItem(`oders`)) || [] )
    }, [])
    useEffect(() => {
        localStorage.setItem(`oders`, JSON.stringify(oders))
    }, [oders])
    useEffect(() => {
        setStarList(JSON.parse(localStorage.getItem(`star`)) || []  )
    }, [])
    useEffect(() => {
        localStorage.setItem(`star`, JSON.stringify(starList))
    }, [starList])

const approveDeleteOdFunc = (item)=>{
    setApproveDeleteOders(item)
}
    
    const deleteOder = (item) => {
        setOders([...oders].filter((el) => el.id !== item.id))
    }
    const deleteStarList = (item) => {
        setStarList([...starList].filter((el) => el.id !== item.id))
    }
    const starListAdd = (item) => {
        let add = false;
        starList.forEach((el) => {
            if (el.id === item.id) {
                add = true
            }
        })
        !add &&
            setStarList([...starList, item]);
    }
    const clearMainChoose = (item) => {
        SetMainOders([])
    }
    const mainChoose = (item) => {
        SetMainOders([item]);

    }

    const Choose = (item) => {
        let add = false;
        oders.forEach((el) => {
            if (el.id === item.id) {
                add = true
            }
        })
        !add && setOders([...oders, item]);
    }

    useEffect(() => {
        const fetchItems = async () => {
            try {
                const response = await fetch(`/getItem.json`);
                const data = await response.json()
                setItem(data.item)
            } catch (er) {
                console.warn(er)
            }
        };
        fetchItems();
    }, [])
    return (<>
        {item && <div className="App">
           <Routes>
            <Route path='/' element={<Header  oders={oders}
                 starList={starList} 
                 deleteOder={deleteOder}
                 deleteStarList={deleteStarList} />}>
                <Route index path='/' element={
                <Card Item={item}
                    Choose={Choose}
                    clearMainChoose={clearMainChoose}
                    mainChoose={mainChoose}
                    mainoders={mainoders}
                    starListAdd={starListAdd}
                    starList={starList}
                    deleteStarList={deleteStarList}
                    oders={oders}
              
                 
                    />}/>
                <Route path='/Shop' element={ <Oders 
                item={oders} 
                deleteOder={deleteOder}
                approveDeleteOders={approveDeleteOders}
                approveDeleteOdFunc={approveDeleteOdFunc}
                />   }/>
                <Route path='/Star' element={ <Star item={starList} deleteStarList={deleteStarList} />}/>
            </Route>
           </Routes>
        </div>}
    </>
    );
}


export default App;
